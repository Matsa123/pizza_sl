$(document).ready(function () {

    var arrImg = $('.pizza');
    var actImgIndex = $('.pizza.act').index();
    var nextImgIndex = actImgIndex + 1;
    var prevImgIndex;
    if (actImgIndex == 0) {
        prevImgIndex = (arrImg.length - 1);
    } else {
        prevImgIndex = actImgIndex - 1;
    }
    var nextImg = $('.pizza').eq(nextImgIndex);
    var prevImg = $('.pizza').eq(prevImgIndex);
    nextImg.css("transform", "translateX(700px)");
    prevImg.css("transform", "translateX(-700px)");
    nextImg.fadeOut(1);
    prevImg.fadeOut(1);

    $('.next').click(function () {

        var arrImg = $('.pizza');
        var actImg = $('.pizza.act');
        var actImgIndex = $('.pizza.act').index();
        var nextImgIndex = actImgIndex + 1;
        var nextImg = $('.pizza').eq(nextImgIndex);

        if (nextImgIndex == arrImg.length - 1) {
            setTimeout(function () {
                $('.pizza').eq(0).css("transform", "translateX(700px)");
                nextImg.fadeIn(1);
                nextImg.css("transform", "translateX(0)");
                actImg.css("transform", "translateX(-700px)");
                actImg.fadeOut(1000);
                actImg.removeClass('act');
                nextImg.addClass('act');
            }, 500);
        }
        if (actImgIndex == arrImg.length - 1) {
            setTimeout(function () {
                $('.pizza').eq(0).fadeIn(1);
                $('.pizza').eq(0).css("transform", "translateX(0)");
                actImg.css("transform", "translateX(-700px)");
                actImg.fadeOut(1000);
                actImg.removeClass('act');
                $('.pizza').eq(0).addClass('act');
                $('.pizza').eq((arrImg.length - 2)).css("transform", "translateX(700px)");
            }, 500);
        }
        else {
            setTimeout(function () {
                nextImg.fadeIn(1);
                nextImg.css("transform", "translateX(0)");
                actImg.css("transform", "translateX(-700px)");
                actImg.fadeOut(1000);

                if (actImg.index() == 0) {
                    $('.pizza').eq((arrImg.length - 1)).css("transform", "translateX(700px)");
                }
                actImg.removeClass('act');
                nextImg.addClass('act');
            }, 500);
        }

    });

    $('.prev').click(function () {
        
        var arrImg = $('.pizza');
        var actImg = $('.pizza.act');
        var actImgIndex = $('.pizza.act').index();
        var prevImgIndex;
        if (actImgIndex == 0) {
            prevImgIndex = (arrImg.length - 1);
        } else {
            prevImgIndex = actImgIndex - 1;
        }
        var prevImg = $('.pizza').eq(prevImgIndex);

        if (actImgIndex == 0) {
            setTimeout(function () {
                prevImg.fadeIn(1);
                prevImg.css("transform", "translateX(0)");
                actImg.css("transform", "translateX(700px)");
                actImg.fadeOut(1000);
                $('.pizza').eq(arrImg.length - 2).css("transform", "translateX(-700px)");
                actImg.removeClass('act');
                prevImg.addClass('act');
            }, 500);
        }
        setTimeout(function () {
            prevImg.fadeIn(1);
            prevImg.css("transform", "translateX(0)");
            actImg.css("transform", "translateX(700px)");
            actImg.fadeOut(1000);
            $('.pizza').eq(prevImgIndex - 1).css("transform", "translateX(-700px)");
            actImg.removeClass('act');
            prevImg.addClass('act');
        }, 500);
    });


    setInterval(() => setTimeout(function () { $('.next').click() }, 500), 15000);



})
