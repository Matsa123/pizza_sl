window.onload = function () {

   //массив елементов, которые будем перемещать на корж пиццы
   let products = document.querySelectorAll("#product");

   //перебираем, вычисляем елемент при зажатии
   products.forEach(e => {
      e.addEventListener('dragstart', function (evt) {
         //настраиваем информацию, которую передадим в перемещаемый елемент
         evt.dataTransfer.setData("Text", this.classList);
      }, false)
   });

   //создаем корж пиццы, на который будем закидывать елементы
   let korj = document.querySelector(".korj");

   //если залетели с зажатым елементом на корж
   korj.addEventListener("dragenter", function (evt) {
      this.style.transform = "scale(1.01)";
   }, false);
   
   //если покинули корж
   korj.addEventListener("dragleave", function (evt) {
      this.style.transform = "";
   }, false);

   korj.addEventListener("dragover", function (evt) {
      if (evt.preventDefault) evt.preventDefault();
      return false;
   }, false);
   //когда сбросили зажатый елемент на корж то делаем...
   korj.addEventListener("drop", function (evt) {
      //убираем лишние маневры по умолчанию
      if (evt.preventDefault) evt.preventDefault();
      if (evt.stopPropagation) evt.stopPropagation();
      //принимаем информацию, которую настраивали выше
      let e = evt.dataTransfer.getData("Text");
      //создаем массив из начинок на корже
      let korjmass = document.querySelectorAll('.korj div')
      //вычисляем какая начинка и что надо добавить и что убрать при необходимости
      if (e == "tomato_photo") {
         korjmass.forEach(el => {
            if (el.classList.contains("tomat_korj")) {
               return;
            }
            if (el.classList.contains("chesnoch_korj")) {
               document.querySelector('.chesnoch_korj').style.opacity = "0";
               pizza.removeStuffing(Pizza.SOUS_CHESNOCH);
            }
            if (el.classList.contains("barbeku_korj")) {
               document.querySelector('.barbeku_korj').style.opacity = "0";
               pizza.removeStuffing(Pizza.SOUS_BARBEKU);
            }
         });
         document.querySelector('.tomat_korj').style.opacity = "1";
         pizza.addStuffing(Pizza.SOUS_TOMATO);
         cena.innerHTML = pizza.calculatePrice();
         tomCounter++;
         if (chesCounter == 1) {
            chesCounter--;
            chesKol.innerHTML = chesCounter;
         }
         if (barbCounter == 1) {
            barbCounter--;
            barbKol.innerHTML = barbCounter;
         }
         tomKol.innerHTML = tomCounter;

      }
      else if (e == "chesnoch_photo") {
         korjmass.forEach(el => {
            if (el.classList.contains("tomat_korj")) {
               document.querySelector('.tomat_korj').style.opacity = "0";
               pizza.removeStuffing(Pizza.SOUS_TOMATO);
            }
            if (el.classList.contains("chesnoch_korj")) {
               return;
            }
            if (el.classList.contains("barbeku_korj")) {
               document.querySelector('.barbeku_korj').style.opacity = "0";
               pizza.removeStuffing(Pizza.SOUS_BARBEKU);
            }
         });
         document.querySelector('.chesnoch_korj').style.opacity = "1";
         pizza.addStuffing(Pizza.SOUS_CHESNOCH);
         cena.innerHTML = pizza.calculatePrice();
         chesCounter++;
         if (tomCounter == 1) {
            tomCounter--;
            tomKol.innerHTML = tomCounter;
         }
         if (barbCounter == 1) {
            barbCounter--;
            barbKol.innerHTML = barbCounter;
         }
         chesKol.innerHTML = chesCounter;
      }
      else if (e == "barbeku_photo") {
         korjmass.forEach(el => {
            if (el.classList.contains("tomat_korj")) {
               document.querySelector('.tomat_korj').style.opacity = "0";
               pizza.removeStuffing(Pizza.SOUS_TOMATO);
            }
            if (el.classList.contains("chesnoch_korj")) {
               document.querySelector('.chesnoch_korj').style.opacity = "0";
               pizza.removeStuffing(Pizza.SOUS_CHESNOCH);
            }
            if (el.classList.contains("barbeku_korj")) {
               return;
            }
         });
         document.querySelector('.barbeku_korj').style.opacity = "1";
         pizza.addStuffing(Pizza.SOUS_BARBEKU);
         cena.innerHTML = pizza.calculatePrice();
         barbCounter++;
         if (tomCounter == 1) {
            tomCounter--;
            tomKol.innerHTML = tomCounter;
         }
         if (chesCounter == 1) {
            chesCounter--;
            chesKol.innerHTML = chesCounter;
         }
         barbKol.innerHTML = barbCounter;
      }
      else if (e == "cheese_photo") {
         if (cheeseCounter == 1) {
            return;
         }
         document.querySelector('.cheese_korj').style.opacity = "1";
         pizza.addStuffing(Pizza.CHEESE);
         cena.innerHTML = pizza.calculatePrice();
         cheeseCounter++;
         cheeseKol.innerHTML = cheeseCounter;
      }
      else if (e == "salami_photo") {
         if (salamiCounter == 1) {
            return;
         }
         document.querySelector('.salami_korj').style.opacity = "1";
         pizza.addStuffing(Pizza.SALAMI);
         cena.innerHTML = pizza.calculatePrice();
         salamiCounter++;
         salamiKol.innerHTML = salamiCounter;
      }
      else if (e == "perec_photo") {
         if (perecCounter == 1) {
            return;
         }
         document.querySelector('.perec_korj').style.opacity = "1";
         pizza.addStuffing(Pizza.PEREC);
         cena.innerHTML = pizza.calculatePrice();
         perecCounter++;
         perecKol.innerHTML = perecCounter;
      }
      else if (e == "chili_photo") {
         if (chiliCounter == 1) {
            return;
         }
         document.querySelector('.chili_korj').style.opacity = "1";
         pizza.addStuffing(Pizza.CHILI);
         cena.innerHTML = pizza.calculatePrice();
         chiliCounter++;
         chiliKol.innerHTML = chiliCounter;
      }
      else if (e == "kukuruza_photo") {
         if (kukuruzaCounter == 1) {
            return;
         }
         document.querySelector('.kukuruza_korj').style.opacity = "1";
         pizza.addStuffing(Pizza.KUKURUZA);
         cena.innerHTML = pizza.calculatePrice();
         kukuruzaCounter++;
         kukuruzaKol.innerHTML = kukuruzaCounter;
      }
      else if (e == "ananas_photo") {
         if (ananasCounter == 1) {
            return;
         }
         document.querySelector('.ananas_korj').style.opacity = "1";
         pizza.addStuffing(Pizza.ANANAS);
         cena.innerHTML = pizza.calculatePrice();
         ananasCounter++;
         ananasKol.innerHTML = ananasCounter;
      }
      else if (e == "chicken_photo") {
         if (chickenCounter == 1) {
            return;
         }
         document.querySelector('.chicken_korj').style.opacity = "1";
         pizza.addStuffing(Pizza.CHICKEN);
         cena.innerHTML = pizza.calculatePrice();
         chickenCounter++;
         chickenKol.innerHTML = chickenCounter;
      }
      this.style.transform = "";
      return false;
   }, false);

   //создаем массив инпутов (для размеров пиццы)
   let inputs = document.querySelectorAll('input');
   //вычисляем нужные инпуты(выбор размера пиццы), если такой размер, то такая цена...
   inputs.forEach(e => {
      e.addEventListener('click', function (evt) {
         let cena = document.querySelector('.itogCena');
         if (e.id == "razm1") {
            pizza.size = Pizza.SIZE_SMALL;
            cena.innerHTML = pizza.calculatePrice();
         } else if (e.id == "razm2") {
            pizza.size = Pizza.SIZE_AVERAGE;
            cena.innerHTML = pizza.calculatePrice();
         } else if (e.id == "razm3") {
            pizza.size = Pizza.SIZE_BIG;
            cena.innerHTML = pizza.calculatePrice();
         }
      });
   });

   //создаем конструктор Пиццы
   function Pizza(size) {
      this.size = size;
   };
   //создаем параметры пиццы
   function Parameter(type, name, cost) {
      this.getType = type;
      this.getName = name;
      this.getCost = cost;
   };

   //создаем начинки используя конструктор параметр
   Pizza.SIZE_SMALL = new Parameter("size", "small", 50);
   Pizza.SIZE_AVERAGE = new Parameter("size", "average", 75);
   Pizza.SIZE_BIG = new Parameter("size", "big", 100);
   Pizza.SOUS_TOMATO = new Parameter("stuffing", "sous_tomato", 20);
   Pizza.SOUS_CHESNOCH = new Parameter("stuffing", "sous_chesnoch", 20);
   Pizza.SOUS_BARBEKU = new Parameter("stuffing", "sous_barbeku", 20);
   Pizza.CHEESE = new Parameter("stuffing", "cheese", 15);
   Pizza.SALAMI = new Parameter("stuffing", "salami", 15);
   Pizza.PEREC = new Parameter("stuffing", "perec", 15);
   Pizza.CHILI = new Parameter("stuffing", "chili", 15);
   Pizza.KUKURUZA = new Parameter("stuffing", "kukuruza", 15);
   Pizza.ANANAS = new Parameter("stuffing", "ananas", 15);
   Pizza.CHICKEN = new Parameter("stuffing", "chicken", 15);

   //создаем функцию добавить начинку в пиццу
   Pizza.prototype.addStuffing = function (stuffing) {
      if (stuffing.getName == "sous_tomato") {
         this.addTomato = stuffing;
         stuffing.added = true;
      } else if (stuffing.getName == "sous_chesnoch") {
         this.addChesnoch = stuffing;
         stuffing.added = true;
      } else if (stuffing.getName == "sous_barbeku") {
         this.addBarbeku = stuffing;
         stuffing.added = true;
      } else if (stuffing.getName == "cheese") {
         this.addCheese = stuffing;
         stuffing.added = true;
      } else if (stuffing.getName == "salami") {
         this.addSalami = stuffing;
         stuffing.added = true;
      } else if (stuffing.getName == "perec") {
         this.addPerec = stuffing;
         stuffing.added = true;
      } else if (stuffing.getName == "chili") {
         this.addChili = stuffing;
         stuffing.added = true;
      } else if (stuffing.getName == "kukuruza") {
         this.addKukuruza = stuffing;
         stuffing.added = true;
      } else if (stuffing.getName == "ananas") {
         this.addAnanas = stuffing;
         stuffing.added = true;
      } else if (stuffing.getName == "chicken") {
         this.addChicken = stuffing;
         stuffing.added = true;
      }
   };
   //создаем функцию убрать начинку из пиццы
   Pizza.prototype.removeStuffing = function (stuffing) {
      if (stuffing.getName == "sous_tomato") {
         delete this.addTomato;
      } else if (stuffing.getName == "sous_chesnoch") {
         delete this.addChesnoch;
      } else if (stuffing.getName == "sous_barbeku") {
         delete this.addBarbeku;
      } else if (stuffing.getName == "cheese") {
         delete this.addCheese;
      } else if (stuffing.getName == "salami") {
         delete this.addSalami;
      } else if (stuffing.getName == "perec") {
         delete this.addPerec;
      } else if (stuffing.getName == "chili") {
         delete this.addChili;
      } else if (stuffing.getName == "kukuruza") {
         delete this.addKukuruza;
      } else if (stuffing.getName == "ananas") {
         delete this.addAnanas;
      } else if (stuffing.getName == "chicken") {
         delete this.addChicken;
      }
   };
   //создаем функцию получить все начинки из пиццы
   Pizza.prototype.getStuffings = function () {
      let stuffArr = [], i = 0;
      for (let item in this) {
         if (this[item].added === true) {
            stuffArr[i] = this[item].getName;
            i++;
         }
      }
      return stuffArr;
   };
   //создаем функцию получить размер пиццы
   Pizza.prototype.getSize = function () {
      return this.size
   };
   //создаем функцию получить начинку пиццы
   Pizza.prototype.getStuffing = function () {
      return this.stuffing.getName
   };
   //создаем функцию подсчета цены пиццы
   Pizza.prototype.calculatePrice = function () {
      let price = 0;
      for (let item in this) {
         if (this[item].getCost) {
            price = this[item].getCost + price;
         }
      }
      return price;

   };
   //находим кнопку сброса
   let sbros = document.querySelector('.sbros');
   //создаем функцию сброса
   sbros.addEventListener("click", () => {
      document.querySelector('.tomat_korj').style.opacity = "0";
      document.querySelector('.chesnoch_korj').style.opacity = "0";
      document.querySelector('.barbeku_korj').style.opacity = "0";
      document.querySelector('.cheese_korj').style.opacity = "0";
      document.querySelector('.salami_korj').style.opacity = "0";
      document.querySelector('.perec_korj').style.opacity = "0";
      document.querySelector('.chili_korj').style.opacity = "0";
      document.querySelector('.kukuruza_korj').style.opacity = "0";
      document.querySelector('.ananas_korj').style.opacity = "0";
      document.querySelector('.chicken_korj').style.opacity = "0";

      pizza.removeStuffing(Pizza.SOUS_TOMATO);
      pizza.removeStuffing(Pizza.SOUS_CHESNOCH);
      pizza.removeStuffing(Pizza.SOUS_BARBEKU);
      pizza.removeStuffing(Pizza.CHEESE);
      pizza.removeStuffing(Pizza.SALAMI);
      pizza.removeStuffing(Pizza.PEREC);
      pizza.removeStuffing(Pizza.CHILI);
      pizza.removeStuffing(Pizza.KUKURUZA);
      pizza.removeStuffing(Pizza.ANANAS);
      pizza.removeStuffing(Pizza.CHICKEN);

      cena.innerHTML = pizza.calculatePrice();

      tomCounter = 0;
      tomKol.innerHTML = tomCounter;
      chesCounter = 0;
      chesKol.innerHTML = chesCounter;
      barbCounter = 0;
      barbKol.innerHTML = barbCounter;
      cheeseCounter = 0;
      cheeseKol.innerHTML = cheeseCounter;
      salamiCounter = 0;
      salamiKol.innerHTML = salamiCounter;
      perecCounter = 0;
      perecKol.innerHTML = perecCounter;
      chiliCounter = 0;
      chiliKol.innerHTML = chiliCounter;
      kukuruzaCounter = 0;
      kukuruzaKol.innerHTML = kukuruzaCounter;
      ananasCounter = 0;
      ananasKol.innerHTML = ananasCounter;
      chickenCounter = 0;
      chickenKol.innerHTML = chickenCounter;
   });
   //находим елементы, для осуществления счетчика итальянского соуса
   let tomMinus = document.querySelector('.tom_minus');
   let tomKol = document.querySelector('.tom_kol');
   let tomPlus = document.querySelector('.tom_plus');
   //создаем счетчик
   let tomCounter = 0;
   tomKol.innerHTML = tomCounter;
   //при клике на минус
   tomMinus.addEventListener("click", () => {
      if (tomCounter == 0) {
         return;
      }
      document.querySelector('.tomat_korj').style.opacity = "0";
      pizza.removeStuffing(Pizza.SOUS_TOMATO);
      cena.innerHTML = pizza.calculatePrice();
      tomCounter--;
      tomKol.innerHTML = tomCounter;
   });
   //при клике на плюс
   tomPlus.addEventListener("click", () => {
      if (tomCounter == 1) {
         return;
      }
      if (document.querySelector('.chesnoch_korj').style.opacity = "1") {
         document.querySelector('.chesnoch_korj').style.opacity = "0"
         pizza.removeStuffing(Pizza.SOUS_CHESNOCH);
      }
      if (document.querySelector('.barbeku_korj').style.opacity = "1") {
         document.querySelector('.barbeku_korj').style.opacity = "0"
         pizza.removeStuffing(Pizza.SOUS_BARBEKU);
      }
      document.querySelector('.tomat_korj').style.opacity = "1";
      pizza.addStuffing(Pizza.SOUS_TOMATO);
      cena.innerHTML = pizza.calculatePrice();
      tomCounter++;
      if (chesCounter == 1) {
         chesCounter--;
         chesKol.innerHTML = chesCounter;
      }
      if (barbCounter == 1) {
         barbCounter--;
         barbKol.innerHTML = barbCounter;
      }
      tomKol.innerHTML = tomCounter;
   });
   //находим елементы, для осуществления счетчика чесночного соуса
   let chesMinus = document.querySelector('.ches_minus');
   let chesKol = document.querySelector('.ches_kol');
   let chesPlus = document.querySelector('.ches_plus');
   
   let chesCounter = 0;
   chesKol.innerHTML = chesCounter;

   chesMinus.addEventListener("click", () => {
      if (chesCounter == 0) {
         return;
      }
      document.querySelector('.chesnoch_korj').style.opacity = "0";
      pizza.removeStuffing(Pizza.SOUS_CHESNOCH);
      cena.innerHTML = pizza.calculatePrice();
      chesCounter--;
      chesKol.innerHTML = chesCounter;
   });
   chesPlus.addEventListener("click", () => {
      if (chesCounter == 1) {
         return;
      }
      if (document.querySelector('.tomat_korj').style.opacity = "1") {
         document.querySelector('.tomat_korj').style.opacity = "0"
         pizza.removeStuffing(Pizza.SOUS_TOMATO);
      }
      if (document.querySelector('.barbeku_korj').style.opacity = "1") {
         document.querySelector('.barbeku_korj').style.opacity = "0"
         pizza.removeStuffing(Pizza.SOUS_BARBEKU);
      }
      document.querySelector('.chesnoch_korj').style.opacity = "1";
      pizza.addStuffing(Pizza.SOUS_CHESNOCH);
      cena.innerHTML = pizza.calculatePrice();
      chesCounter++;
      if (tomCounter == 1) {
         tomCounter--;
         tomKol.innerHTML = tomCounter;
      }
      if (barbCounter == 1) {
         barbCounter--;
         barbKol.innerHTML = barbCounter;
      }
      chesKol.innerHTML = chesCounter;
   });
   //находим елементы, для осуществления счетчика соуса барбекю
   let barbMinus = document.querySelector('.barb_minus');
   let barbKol = document.querySelector('.barb_kol');
   let barbPlus = document.querySelector('.barb_plus');

   let barbCounter = 0;
   barbKol.innerHTML = barbCounter;

   barbMinus.addEventListener("click", () => {
      if (barbCounter == 0) {
         return;
      }
      document.querySelector('.barbeku_korj').style.opacity = "0";
      pizza.removeStuffing(Pizza.SOUS_BARBEKU);
      cena.innerHTML = pizza.calculatePrice();
      barbCounter--;
      barbKol.innerHTML = barbCounter;
   });
   barbPlus.addEventListener("click", () => {
      if (barbCounter == 1) {
         return;
      }
      if (document.querySelector('.tomat_korj').style.opacity = "1") {
         document.querySelector('.tomat_korj').style.opacity = "0"
         pizza.removeStuffing(Pizza.SOUS_TOMATO);
      }
      if (document.querySelector('.chesnoch_korj').style.opacity = "1") {
         document.querySelector('.chesnoch_korj').style.opacity = "0"
         pizza.removeStuffing(Pizza.SOUS_CHESNOCH);
      }
      document.querySelector('.barbeku_korj').style.opacity = "1";
      pizza.addStuffing(Pizza.SOUS_BARBEKU);
      cena.innerHTML = pizza.calculatePrice();
      barbCounter++;
      if (tomCounter == 1) {
         tomCounter--;
         tomKol.innerHTML = tomCounter;
      }
      if (chesCounter == 1) {
         chesCounter--;
         chesKol.innerHTML = chesCounter;
      }
      barbKol.innerHTML = barbCounter;
   });
   //находим елементы, для осуществления счетчика сыра моцаррелла
   let cheeseMinus = document.querySelector('.cheese_minus');
   let cheeseKol = document.querySelector('.cheese_kol');
   let cheesePlus = document.querySelector('.cheese_plus');

   let cheeseCounter = 0;
   cheeseKol.innerHTML = cheeseCounter;

   cheeseMinus.addEventListener("click", () => {
      if (cheeseCounter == 0) {
         return;
      }
      document.querySelector('.cheese_korj').style.opacity = "0";
      pizza.removeStuffing(Pizza.CHEESE);
      cena.innerHTML = pizza.calculatePrice();
      cheeseCounter--;
      cheeseKol.innerHTML = cheeseCounter;
   });
   cheesePlus.addEventListener("click", () => {
      if (cheeseCounter == 1) {
         return;
      }
      document.querySelector('.cheese_korj').style.opacity = "1";
      pizza.addStuffing(Pizza.CHEESE);
      cena.innerHTML = pizza.calculatePrice();
      cheeseCounter++;
      cheeseKol.innerHTML = cheeseCounter;
   });
   //находим елементы, для осуществления счетчика салями
   let salamiMinus = document.querySelector('.salami_minus');
   let salamiKol = document.querySelector('.salami_kol');
   let salamiPlus = document.querySelector('.salami_plus');

   let salamiCounter = 0;
   salamiKol.innerHTML = salamiCounter;

   salamiMinus.addEventListener("click", () => {
      if (salamiCounter == 0) {
         return;
      }
      document.querySelector('.salami_korj').style.opacity = "0";
      pizza.removeStuffing(Pizza.SALAMI);
      cena.innerHTML = pizza.calculatePrice();
      salamiCounter--;
      salamiKol.innerHTML = salamiCounter;
   });
   salamiPlus.addEventListener("click", () => {
      if (salamiCounter == 1) {
         return;
      }
      document.querySelector('.salami_korj').style.opacity = "1";
      pizza.addStuffing(Pizza.SALAMI);
      cena.innerHTML = pizza.calculatePrice();
      salamiCounter++;
      salamiKol.innerHTML = salamiCounter;
   });
   //находим елементы, для осуществления счетчика перца болгарского
   let perecMinus = document.querySelector('.perec_minus');
   let perecKol = document.querySelector('.perec_kol');
   let perecPlus = document.querySelector('.perec_plus');

   let perecCounter = 0;
   perecKol.innerHTML = perecCounter;

   perecMinus.addEventListener("click", () => {
      if (perecCounter == 0) {
         return;
      }
      document.querySelector('.perec_korj').style.opacity = "0";
      pizza.removeStuffing(Pizza.PEREC);
      cena.innerHTML = pizza.calculatePrice();
      perecCounter--;
      perecKol.innerHTML = perecCounter;
   });
   perecPlus.addEventListener("click", () => {
      if (perecCounter == 1) {
         return;
      }
      document.querySelector('.perec_korj').style.opacity = "1";
      pizza.addStuffing(Pizza.PEREC);
      cena.innerHTML = pizza.calculatePrice();
      perecCounter++;
      perecKol.innerHTML = perecCounter;
   });
   //находим елементы, для осуществления счетчика перца чили
   let chiliMinus = document.querySelector('.chili_minus');
   let chiliKol = document.querySelector('.chili_kol');
   let chiliPlus = document.querySelector('.chili_plus');

   let chiliCounter = 0;
   chiliKol.innerHTML = chiliCounter;

   chiliMinus.addEventListener("click", () => {
      if (chiliCounter == 0) {
         return;
      }
      document.querySelector('.chili_korj').style.opacity = "0";
      pizza.removeStuffing(Pizza.CHILI);
      cena.innerHTML = pizza.calculatePrice();
      chiliCounter--;
      chiliKol.innerHTML = chiliCounter;
   });
   chiliPlus.addEventListener("click", () => {
      if (chiliCounter == 1) {
         return;
      }
      document.querySelector('.chili_korj').style.opacity = "1";
      pizza.addStuffing(Pizza.CHILI);
      cena.innerHTML = pizza.calculatePrice();
      chiliCounter++;
      chiliKol.innerHTML = chiliCounter;
   });
   //находим елементы, для осуществления счетчика кукурузы
   let kukuruzaMinus = document.querySelector('.kukuruza_minus');
   let kukuruzaKol = document.querySelector('.kukuruza_kol');
   let kukuruzaPlus = document.querySelector('.kukuruza_plus');

   let kukuruzaCounter = 0;
   kukuruzaKol.innerHTML = kukuruzaCounter;

   kukuruzaMinus.addEventListener("click", () => {
      if (kukuruzaCounter == 0) {
         return;
      }
      document.querySelector('.kukuruza_korj').style.opacity = "0";
      pizza.removeStuffing(Pizza.KUKURUZA);
      cena.innerHTML = pizza.calculatePrice();
      kukuruzaCounter--;
      kukuruzaKol.innerHTML = kukuruzaCounter;
   });
   kukuruzaPlus.addEventListener("click", () => {
      if (kukuruzaCounter == 1) {
         return;
      }
      document.querySelector('.kukuruza_korj').style.opacity = "1";
      pizza.addStuffing(Pizza.CHILI);
      cena.innerHTML = pizza.calculatePrice();
      kukuruzaCounter++;
      kukuruzaKol.innerHTML = kukuruzaCounter;
   });
   //находим елементы, для осуществления счетчика ананаса
   let ananasMinus = document.querySelector('.ananas_minus');
   let ananasKol = document.querySelector('.ananas_kol');
   let ananasPlus = document.querySelector('.ananas_plus');

   let ananasCounter = 0;
   ananasKol.innerHTML = ananasCounter;

   ananasMinus.addEventListener("click", () => {
      if (ananasCounter == 0) {
         return;
      }
      document.querySelector('.ananas_korj').style.opacity = "0";
      pizza.removeStuffing(Pizza.ANANAS);
      cena.innerHTML = pizza.calculatePrice();
      ananasCounter--;
      ananasKol.innerHTML = ananasCounter;
   });
   ananasPlus.addEventListener("click", () => {
      if (ananasCounter == 1) {
         return;
      }
      document.querySelector('.ananas_korj').style.opacity = "1";
      pizza.addStuffing(Pizza.ANANAS);
      cena.innerHTML = pizza.calculatePrice();
      ananasCounter++;
      ananasKol.innerHTML = ananasCounter;
   });
   //находим елементы, для осуществления счетчика курицы
   let chickenMinus = document.querySelector('.chicken_minus');
   let chickenKol = document.querySelector('.chicken_kol');
   let chickenPlus = document.querySelector('.chicken_plus');

   let chickenCounter = 0;
   chickenKol.innerHTML = chickenCounter;

   chickenMinus.addEventListener("click", () => {
      if (chickenCounter == 0) {
         return;
      }
      document.querySelector('.chicken_korj').style.opacity = "0";
      pizza.removeStuffing(Pizza.CHICKEN);
      cena.innerHTML = pizza.calculatePrice();
      chickenCounter--;
      chickenKol.innerHTML = chickenCounter;
   });
   chickenPlus.addEventListener("click", () => {
      if (chickenCounter == 1) {
         return;
      }
      document.querySelector('.chicken_korj').style.opacity = "1";
      pizza.addStuffing(Pizza.CHICKEN);
      cena.innerHTML = pizza.calculatePrice();
      chickenCounter++;
      chickenKol.innerHTML = chickenCounter;
   });
   //создаем пиццу
   let pizza = new Pizza(Pizza.SIZE_SMALL);

   let cena = document.querySelector('.itogCena');
   cena.innerHTML = pizza.calculatePrice();
   //находим елементы формы, при нажатии кнопки Подтвердить
   let enter = document.querySelector('.enter');
   let itogoRazmer = document.querySelector('.itogo_razmer');
   let itogoNachinka = document.querySelector('.itogo_nachinka');
   let itogoCena = document.querySelector('.itogo_cena');
   let box = document.querySelector('#box');
   //когда нажали подтвердить, учитываем информацию что выбрал пользователь и выводим в форму
   enter.addEventListener("click", () => {
      //делаем форму видимой
      box.style.display = "block";
      //вычисляем размер, который выбрал пользователь
      if (pizza.getSize() == Pizza.SIZE_SMALL) {
         itogoRazmer.innerHTML = "<b>маленькая";
      }
      if (pizza.getSize() == Pizza.SIZE_AVERAGE) {
         itogoRazmer.innerHTML = "<b>средняя";
      }
      if (pizza.getSize() == Pizza.SIZE_BIG) {
         itogoRazmer.innerHTML = "<b>большая";
      }
      //создаем масссив из начинок, которые выбрал вользователь
      let arrStuff = pizza.getStuffings();
      //перебираем массив и вычисляем что выбрал пользователь и выводим в форме результат
      arrStuff.forEach(e => {
         if (e == "sous_tomato") {
            itogoNachinka.innerHTML = "<b>итальянский соус</b> <br>" + itogoNachinka.innerHTML;
         }
         if (e == "sous_chesnoch") {
            itogoNachinka.innerHTML = "<b>чесночный соус</b> <br> " + itogoNachinka.innerHTML;
         }
         if (e == "sous_barbeku") {
            itogoNachinka.innerHTML = "<b>соус барбекю</b> <br> " + itogoNachinka.innerHTML;
         }
         if (e == "cheese") {
            itogoNachinka.innerHTML += "<b>сыр моцарелла</b> <br> ";
         }
         if (e == "salami") {
            itogoNachinka.innerHTML += "<b>салями</b> <br> ";
         }
         if (e == "perec") {
            itogoNachinka.innerHTML += "<b>перец болгарский</b> <br> ";
         }
         if (e == "chili") {
            itogoNachinka.innerHTML += "<b>перец чили</b> <br> ";
         }
         if (e == "kukuruza") {
            itogoNachinka.innerHTML += "<b>кукуруза конс.</b> <br> ";
         }
         if (e == "ananas") {
            itogoNachinka.innerHTML += "<b>ананас конс.</b> <br> ";
         }
         if (e == "chicken") {
            itogoNachinka.innerHTML += "<b>курица</b> <br> ";
         }

      });
      //выводим итог цену
      itogoCena.innerHTML = "<b>" + pizza.calculatePrice() + " грн<b>";
      //создаем елемент закрытия формы
      let x = document.querySelector('.x');
      x.addEventListener("click", () => {
         box.style.display = "none";
         itogoRazmer.innerHTML = "";
         itogoNachinka.innerHTML = "";
      });
   });
   //создаем форму, вычисляем ее елементы
   let form = document.querySelector('.form');
   const name = form.elements[0],
         email = form.elements[1],
         tel = form.elements[2];
   //при нажатии на Отправить, проверим введены ли данные
   form.addEventListener("submit", (e) => {
      let isValid = true;
      
      if (name.value.length == 0) {
          isValid = false;
      }
      if (email.value.length == 0) {
          isValid = false;
      }
      if (tel.value.length == 0) {
         isValid = false;
     }
      if (!isValid) {
          e.preventDefault();
      }
  });
}
